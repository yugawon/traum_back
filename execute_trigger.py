# -*- coding: utf-8 -*-

import os
import cv2
import glob
import time
import pymysql
import numpy as np
import configparser
from datetime import datetime
import matplotlib.image as mpimg
from sklearn.cluster import KMeans
from matplotlib import pyplot as plt

CNF_INI = './config.ini'

INAME = datetime.today().strftime("%Y%m%d%H%M")

config = configparser.ConfigParser()
config.read(CNF_INI)
HOST = config['mysql']['host']
USER = config['mysql']['user']
PASSWORD = config['mysql']['password']
DATABASE = config['mysql']['database']
PORT = config['mysql']['port']
CHARSET = config['mysql']['charset']

UIMAGE = config['image']['fupload']
DUPALETTE = config['image']['dupalette']
DWPALETTE = config['image']['dwpalette']
DWALLPAPER = config['image']['dwallpaper']
DUPLOAD = config['image']['dupload']
CORR = config['value']['correction']
K = config['value']['k']

"""UPLOAD IMAGE"""

def trigger_detection():
  constant = []

  constant.append(UIMAGE)
  constant.append(DUPALETTE)
  constant.append(DWPALETTE)
  constant.append(DWALLPAPER)
  constant.append(DUPLOAD)
  constant.append(CORR) 
  constant.append(K)

  return constant

def db_access():
  conn = pymysql.connect(
    host=HOST,
    user=USER,
    password=PASSWORD,
    db=DATABASE,
    charset=CHARSET,
    port=int(PORT),
    cursorclass=pymysql.cursors.DictCursor)
  return conn

def exec_overlap_palette_image(conn,oname,iname):
  with conn.cursor() as cursor:
    query = get_overlap_palette_image(oname,iname)
    cursor.execute(query)
    for row in cursor:
      data = row.get('cnt')
      
  return data

def exec_overlap_palette_result(conn,input,output):
  with conn.cursor() as cursor:
    query = get_overlap_palette_result(input,output)
    cursor.execute(query)
    for row in cursor:
      data = row.get('cnt')
      
  return data

def exec_insert_palette_image(conn,oname,iname,array,rcolor,gcolor,bcolor):
  query = get_insert_palette_image(oname,iname,array,rcolor,gcolor,bcolor)
  with conn.cursor() as cursor:
    cursor.execute(query)
  conn.commit()

def exec_insert_palette_result(conn,input,output,rcolor,gcolor,bcolor):
  query = get_insert_palette_result(input,output,rcolor,gcolor,bcolor)
  with conn.cursor() as cursor:
    cursor.execute(query)
  conn.commit()

def exec_palette_image(conn,image):
  with conn.cursor() as cursor:
    query = get_select_palette_image(image)
    cursor.execute(query)
    data = cursor.fetchall()
    return data

def exec_palette_result(conn,image):
  with conn.cursor() as cursor:
    query = get_select_palette_result(image)
    cursor.execute(query)
    data = cursor.fetchall()
    return data

def get_overlap_palette_image(oname,iname):
  query = 'select \
  count(*) as cnt \
  from palette_image \
  where oname=\'{}\' and iname=\'{}\''.format(oname,iname)
  # print(query)
  return query

def get_overlap_palette_result(input,output):
  query = 'select \
  count(*) as cnt \
  from palette_result \
  where input=\'{}\' and output=\'{}\''.format(input,output)
  # print(query)
  return query

def get_insert_palette_image(oname,iname,array,rcolor,gcolor,bcolor):
  query = 'insert into palette_image (oname,iname,array,rcolor,gcolor,bcolor) \
  values (\'{}\',\'{}\',{},{},{},{})'.format(oname,iname,array,rcolor,gcolor,bcolor)
  # print(query)
  return query

def get_insert_palette_result(input,output,rcolor,gcolor,bcolor):
  query = 'insert into palette_result (input,output,rcolor,gcolor,bcolor) \
  values (\'{}\',\'{}\',{},{},{})'.format(input,output,rcolor,gcolor,bcolor)
  # print(query)
  return query

def get_select_palette_image(image):
  query = 'select * from palette_image where oname = \'{}\''.format(image)
  # print(query)
  return query

def get_select_palette_result(image):
  query = 'select * from palette_result where input = \'{}\''.format(image)
  # print(query)
  return query

def sp_find_palette_image(conn,rminus,rplus,gminus,gplus,bminus,bplus):
  with conn.cursor() as cursor:
    stored_procedure = 'call FindPaletteImage({},{},{},{},{},{})'.format(rminus,rplus,gminus,gplus,bminus,bplus)
    # print(stored_procedure)
    cursor.execute(stored_procedure)
    data = cursor.fetchall()
    return data

def sp_find_palette_wallpaper(conn,rminus,rplus,gminus,gplus,bminus,bplus):
  with conn.cursor() as cursor:
    stored_procedure = 'call FindPaletteWallpaper({},{},{},{},{},{})'.format(rminus,rplus,gminus,gplus,bminus,bplus)
    # print(stored_procedure)
    cursor.execute(stored_procedure)
    data = cursor.fetchall()
    return data

def origin_file(dirs):
  tmp = []
  tmp = dirs.split('\\')
  tmp2 = tmp[len(tmp)-1]

  return tmp2

def index_file(dirs):
  tmp = []
  tmp = dirs.split('\\')
  tmp2 = tmp[len(tmp)-1]
  tmp3 = tmp2.split('.')
  tmp4 = INAME+'.'+tmp3[1]

  return tmp4

def centroid_histogram(clt):
  # grab the number of different clusters and create a histogram
  # based on the number of pixels assigned to each cluster
  numLabels = np.arange(0, len(np.unique(clt.labels_)) + 1)
  (hist, _) = np.histogram(clt.labels_, bins=numLabels)

  # normalize the histogram, such that it sums to one
  hist = hist.astype("float")
  hist /= hist.sum()

  # return the histogram
  return hist

def plot_colors(hist, centroids):
  # initialize the bar chart representing the relative frequency
  # of each of the colors
  bar = np.zeros((50, 300, 3), dtype="uint8")
  startX = 0

  # loop over the percentage of each cluster and the color of
  # each cluster
  for (percent, color) in zip(hist, centroids):
    # plot the relative percentage of each cluster
    endX = startX + (percent * 300)
    cv2.rectangle(bar, (int(startX), 0), (int(endX), 50),
                      color.astype("uint8").tolist(), -1)
    startX = endX

  # return the bar chart
  return bar

def color_palette(save,image_path, palette_uri, k):
  image = cv2.imread(image_path)
  image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
  image = image.reshape((image.shape[0] * image.shape[1], 3))
    
  clt = KMeans(n_clusters = k)
  clt.fit(image)
    
  hist = centroid_histogram(clt)
  bar = plot_colors(hist, clt.cluster_centers_)
    
  plt.figure()
  plt.axis("off")
  #plt.imshow(bar)
  #plt.show()
    
  if save:
    plt.imsave(palette_uri+'palette_'+origin_file(image_path), bar)

def tagging_color(conn, oname, iname, image_path, palette_uri, k):
  image = cv2.imread(image_path)
  image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
  image = image.reshape((image.shape[0] * image.shape[1], 3))
    
  clt = KMeans(n_clusters = k)
  clt.fit(image)
    
  hist = centroid_histogram(clt)
  bar = plot_colors(hist, clt.cluster_centers_)

  _red = []
  _green = []
  _blue = []

  for center in clt.cluster_centers_:
    try:
      _red.append(int(center[0]))
      _green.append(int(center[1]))
      _blue.append(int(center[2]))
    except ValueError:
      _red.append(0)
      _green.append(0)
      _blue.append(0)

    for i in range(len(_red)):
      # print(i,_blue[i])
      #cnt = exec_overlap_palette_image(conn,oname,iname)
      cnt = 0
      
      if cnt:
        print('RGB[{}]: overlap seq: {}'.format(i,cnt))
        # plt.imshow(mpimg.imread(palette_image+'palette_'+oname))
        # plt.show()
      else:
        color_palette(1,image_path, palette_uri ,k)

        for i in range(len(_red)):
          print('RGB[{}]: does not overlap seq: {}'.format(i,cnt))
          exec_insert_palette_image(conn,oname,iname,i,_red[i],_green[i],_blue[i])

def correction(value):
  dict_corrrection = {
    0:[0,255],
    1:[1,254],
    2:[2,253],
    3:[3,252],
    4:[4,251],
    5:[5,250]
  }

  return dict_corrrection.get(value)

def deviation(start, end, color):

  plus, minus = [], []

  if color >= start & color <= end:
    minus.append(color-start)
    plus.append(color+start)
  else:
    minus.append(0)
    plus.append(0)

  dic_color = {}

  dic_color['plus'] = plus  
  dic_color['minus'] = minus    

  return dic_color

def toInteger(value):
  color = str(value).replace('[','').replace(']','')
  return int(color)

def findWallpaper(conn,find,rcolor,rminus,rplus,gcolor,gminus,gplus,bcolor,bminus,bplus):

  oname = []

  # print('red: {} ~ {}, green: {} ~ {}, blue: {} ~ {}'.format(rplus,rminus,gplus,gminus,bplus,bminus))

  data = sp_find_palette_wallpaper(conn,rminus,rplus,gminus,gplus,bminus,bplus)
  # data = sp_find_palette_image(conn,rminus,rplus,gminus,gplus,bminus,bplus)
  
  # print('data: {}'.format(data))

  if data:
    for i in range(len(data)):
      oname.append(data[i]['oname'])
  else:
    oname.append('')
  
  for i in range(len(data)):
    print(oname[i])

  if data:
    for i in range(len(data)):
      # print('IN:{}, R:{}, G:{}, B:{}, OUT:{}'.format(find,rcolor,gcolor,bcolor,oname[i]))
      cnt = exec_overlap_palette_result(conn,find,oname[i])
      # if cnt:
        # print('overlap seq: {}'.format(cnt))
      if cnt == 0:
        exec_insert_palette_result(conn,find,oname[i],rcolor,gcolor,bcolor)

def viewWallpaper(conn,upload_image,palette_image,palette_wallpaper,wallpaper_uri,upload_uri):

  oname = origin_file(upload_image)

  data = exec_palette_result(conn,oname)

  if data:

    print('Upload Image')
    plt.imshow(mpimg.imread(upload_image))
    plt.show()
    plt.imshow(mpimg.imread(palette_image+'palette_'+oname))
    plt.show()
    print()

    for i in range(len(data)):
      print('Similar wallpaper')
      print('[{}] {}, {}, {}, {}, {}'.format(i,data[i]['input'],data[i]['output'],data[i]['rcolor'],data[i]['gcolor'],data[i]['bcolor']))
      plt.imshow(mpimg.imread(wallpaper_uri+data[i]['output']))
      plt.show()
      plt.imshow(mpimg.imread(palette_wallpaper.replace('\\\\','\\')+'palette_'+data[i]['output']))

      plt.show()

"""EXECUTE"""

'''
color = 120
corr = correction(1)
print('start num: {}, end num: {}'.format(corr[0], corr[1]))
devi = deviation(corr[0], corr[1], color)
print(devi)
exit()
END'''

def start(file_name):

  print('Start')

  print('Current time : {}'.format(datetime.today().strftime("%Y-%m-%d %H:%M:%S"))) 

  constant = trigger_detection()
  
  upload_image = '.\\images\\upload\\'+file_name
  palette_image = constant[1]
  palette_wallpaper = constant[2]
  wallpaper_uri = constant[3]
  upload_uri = constant[4]
  corr = constant[5]
  k = constant[6]

  oname = origin_file(upload_image)
  iname = index_file(upload_image)

  #plt.imshow(mpimg.imread(upload_image))
  #plt.show()

  # if os.path.isfile(palette_image+'palette_'+oname):
  #   plt.imshow(mpimg.imread(palette_image+'palette_'+oname))
  #   plt.show()

  conn = db_access()

  print('upload:{}, oname:{} iname:{}'.format(upload_image,oname,iname))
  tagging_color(conn,oname,iname,upload_image,palette_image,int(k))
  print('completed tagging')

  data = exec_palette_image(conn,origin_file(upload_image))

  arr_minus_red, arr_plus_red = [], []

  arr_minus_green, arr_plus_green = [], []

  arr_minus_blue, arr_plus_blue = [], []

  for i in range(len(data)):
    # print('array:{}, rcolor:{}, gcolor:{}, bcolor:{}'.format(data[i]['array'],data[i]['rcolor'],data[i]['gcolor'],data[i]['bcolor']))
    
    rcolor = int(data[i]['rcolor'])
    gcolor = int(data[i]['gcolor'])
    bcolor = int(data[i]['bcolor'])

    # print('red: {}, green: {}, blue: {}'.format(rcolor,gcolor,bcolor))

    _corr = correction(int(corr))
    print('{}, {}'.format(_corr[0],_corr[1]))

    rdeviation = deviation(_corr[0],_corr[1],rcolor)
    gdeviation = deviation(_corr[0],_corr[1],gcolor)
    bdeviation = deviation(_corr[0],_corr[1],bcolor)

    # print('[{}], red: {}, green: {}, blue: {}'.format(i,rdeviation,gdeviation,bdeviation))
    # print('red: [{}], {}, {}'.format(i,toInteger(rdeviation['plus']),toInteger(rdeviation['minus'])))
    # print('green: [{}], {}, {}'.format(i,toInteger(gdeviation['plus']),toInteger(gdeviation['minus'])))
    # print('blue: [{}], {}, {}'.format(i,toInteger(bdeviation['plus']),toInteger(bdeviation['minus'])))

    findWallpaper(conn,origin_file(upload_image),rcolor,toInteger(rdeviation['minus']),toInteger(rdeviation['plus']),gcolor,toInteger(gdeviation['minus']),toInteger(gdeviation['plus']),bcolor,toInteger(bdeviation['minus']),toInteger(bdeviation['plus']))

  #viewWallpaper(conn,upload_image,palette_image,palette_wallpaper,wallpaper_uri,upload_uri)

  print('End')

