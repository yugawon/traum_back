from flask import Flask, request, send_file
from sqlalchemy import create_engine, text
from flask_cors import CORS
from PIL import Image
from datetime import datetime
import base64
import execute_trigger
import json

app = Flask(__name__)
app.config.from_pyfile('config.py')

database = create_engine(app.config['DB_URL'], encoding = 'utf-8')
app.database = database
CORS(app)

@app.route('/')
def test():
    return 'hello'

@app.route('/upload',methods=['POST'])
def upload():
    photo = request.files['file']
    file_name = photo.filename
    
    photo.save('./images/upload/'+file_name)
    execute_trigger.start(file_name)
    return file_name

@app.route('/result', methods=['POST'])
def result_list():
    res = dict()
    file_name = request.form.get('file_name')
    
    dir_origin = './images/upload/'+file_name
    dir_origin_wallpaper = './images/palette/upload/palette_'+file_name
    
    with open(dir_origin, "rb") as origin, open(dir_origin_wallpaper, "rb") as origin_wallpaper:
        ori = base64.b64encode(origin.read()).decode('utf-8')
        ori_wall = base64.b64encode(origin_wallpaper.read()).decode('utf-8')

    res['ori'] = ori
    res['ori_wall'] = ori_wall

    result_list = app.database.execute(text("select output from palette_result where input="+'"'+str(file_name)+'"'))
    r_list = result_list.fetchall()
    result = [item for t in r_list for item in t]
    res['rlist']=result

    color_list = app.database.execute(text("select rcolor,gcolor,bcolor from palette_result where input="+'"'+str(file_name)+'"'))
    c_list = color_list.fetchall()
    color = ["rgb"+str(x) for x in c_list]
    res['color'] = color

    return json.dumps(res)

@app.route('/detail',methods=['POST'])
def get_detail():
    res = dict()
    file_name = request.form.get('file_name')
    result_name = request.form.get('result_name')
    print(result_name)
    directory = './images/wallpaper/'+result_name

    with open(directory, "rb") as wallpaper:
        result1 = base64.b64encode(wallpaper.read()).decode('utf-8')
      

    res['wallpaper'] = result1


    test = app.database.execute(text("select rcolor,gcolor,bcolor from palette_result where output="+'"'+str(result_name)+'"'+"and input="+'"'+file_name+'"'))
    test= test.fetchall()
    t = [str(x) for x in test]
    res['color'] = t

    return json.dumps(res)

if __name__=='__main__':
    app.run(host='0.0.0.0', port=23351, debug=True)