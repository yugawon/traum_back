db = {
    'user': 'root',
    'password':'password',
    'host':'127.0.0.1',
    'port': '33060',
    'database':'cidi'
}

DB_URL = f"mysql+mysqlconnector://{db['user']}:{db['password']}@{db['host']}:{db['port']}/{db['database']}?charset=utf8"